﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Windows;
using DatabaseApplication.Annotations;
using DatabaseApplication.crypto;
using DatabaseApplication.gui.Main;
using DatabaseApplication.io;
using DatabaseApplication.io.entities;
using DatabaseApplication.Net;
using DatabaseApplication.Properties;
using DatabaseApplication.Utils;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DatabaseApplication.login
{
    /// <summary>
    /// A class handling all operations connected with user authentification
    /// </summary>
    public class Login
    {
        public static string Token { get; set; }

        public static async Task<HttpResponseMessage> Authenticate(string userName, string password)
        {
            var pairs = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", userName),
                new KeyValuePair<string, string>("Password", password)
            };
            var content = new FormUrlEncodedContent(pairs);
            try
            {
                using (var client = new HttpClient())
                {
                    var path = ConfigurationManager.AppSettings["path"];
                    var resp = await client.PostAsync(path + "/token", content);
                    //Here comes spaghetti
                    if (!resp.IsSuccessStatusCode)
                    {
                        var err = await resp.Content.ReadAsStringAsync();
                        var errorDictionary =
                            JsonConvert.DeserializeObject<Dictionary<string, string>>(err);
                        try
                        {
                            MessageProvider.Instance.SendMessage(errorDictionary["error_description"]);
                        }
                        catch
                        {
                            try
                            {
                                var s = errorDictionary["error"];
                                if (s == "Email not confirmed")
                                {
                                    throw new AccountNotConfirmedException();
                                }
                                else
                                {
                                    MessageProvider.Instance.SendMessage(s);
                                }
                            }
                            catch (AccountNotConfirmedException)
                            {
                                throw;
                            }
                            catch (Exception)
                            {
                                MessageProvider.Instance.SendMessage("Unknown error");
                            }
                        }
                        return null;
                    }
                    var res = await resp.Content.ReadAsStringAsync();
                    Dictionary<string, string> tokenDictionary =
                        JsonConvert.DeserializeObject<Dictionary<string, string>>(res);
                    Token = tokenDictionary["access_token"];
                    return resp;
                }
            }
            catch (AccountNotConfirmedException)
            {
                throw;
            }
            catch (Exception e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return null;
            }
        }

        internal static bool IsEmailValid(string email)
        {
            return new EmailAddressAttribute().IsValid(email);
                
        }     

        public static async Task<HttpResponseMessage> CreateNewUser(string email, string password)
        {
            var registerModel = new
            {
                Email = email,
                Password = password,
                ConfirmPassword = password
            };
            try
            {
                using (var client = new HttpClient())
                {
                    var path = ConfigurationManager.AppSettings["path"];
                    var resp = await client.PostAsJsonAsync(path + "/api/Account/Create", registerModel);
                    if (!resp.IsSuccessStatusCode)
                    {
                        var res = await resp.Content.ReadAsStringAsync();
                        var errorDictionary =
                            JsonConvert.DeserializeObject<ErrorModel>(res);
                        MessageProvider.Instance.SendMessage(errorDictionary.modelState.Values.First()[0]);
                        return null;
                    }
                    return resp;
                }
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return null;
            }
        }

        public static async Task<bool> ResetPassword(string email)
        {
            var body = new {Username = email};
            var path = ConfigurationManager.AppSettings["path"];
            var Url = path + "/api/Account/Reset";
            try
            {
                using (var client = new HttpClient())
                {
                    var resp = await client.PostAsJsonAsync(Url, body);
                    if (!resp.IsSuccessStatusCode)
                    {
                        var res = await resp.Content.ReadAsStringAsync();
                        var errorDictionary =
                            JsonConvert.DeserializeObject<Dictionary<string, string>>(res);
                        MessageProvider.Instance.SendMessage(errorDictionary.Values.First());
                        return false;
                    }
                    return true;
                }
            }
            catch (Exception e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return false;
            }
        }

        public static async Task<bool> ConfirmReset(string email, string code, string password)
        {
            var confirmModel = new
            {
                Email = email,
                NewPassword = password,
                ConfirmPassword = password,
                Code = code
            };
            try
            {
                using (var client = new HttpClient())
                {
                    var path = ConfigurationManager.AppSettings["path"];
                    var resp = await client.PostAsJsonAsync(path + "/api/Account/ConfirmReset", confirmModel);
                    if (!resp.IsSuccessStatusCode)
                    {
                        var res = await resp.Content.ReadAsStringAsync();
                        var errorDictionary =
                            JsonConvert.DeserializeObject<ErrorModel>(res);
                        MessageProvider.Instance.SendMessage(errorDictionary.modelState.Values.First()[0]);
                        return false;
                    }
                    return true;
                }
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return false;
            }
        }


        public static void Logout()
        {
            Settings.Default.RememberToken = "";
            Settings.Default.Save();
        }

        public static async Task<bool> ConfirmAccount(string code, string username)
        {
            var body = new {Username = username};
            var path = ConfigurationManager.AppSettings["path"];
            var callbackUrl = path + "/api/Account/ConfirmEmail?code=" + code;
            try
            {
                using (var client = new HttpClient())
                {
                    var resp = await client.PostAsJsonAsync(callbackUrl, body);
                    return resp.IsSuccessStatusCode;
                }
            }
            catch (Exception e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return false;
            }
        }

        public static async Task<bool> DeleteAccount(HttpClient client)
        {
            var path = ConfigurationManager.AppSettings["path"] + "/api/pages/";
            try
            {
                var resp = await client.DeleteAsync(path);
                return resp.IsSuccessStatusCode;
            }
            catch (Exception e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return false;
            }
        }

       

    }


    public class EmailNotValidException : Exception
    {
    }
    public class AccountNotConfirmedException : Exception
    {
    }
}