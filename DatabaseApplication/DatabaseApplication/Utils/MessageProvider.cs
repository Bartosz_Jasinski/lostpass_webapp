﻿using System;
using System.Collections.Generic;
using DatabaseApplication.Annotations;

namespace DatabaseApplication.Utils
{

    /// <summary>
    /// Klasa Singletonowa do przesyłania wiadomości
    /// Można sie podpisywać pod OnReceived
    /// </summary>
    public class MessageProvider : IMessageProvider
    {
        private static MessageProvider _instance;

        private MessageProvider()
        {
        }

        public static MessageProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MessageProvider();
                }
                return _instance;
            }
        }

        public Stack<Message> Messages { get; } = new Stack<Message>();
        public event Action<Message> OnReceived;

        public void SendMessage(string messageText)
        {
            SendMessage(new Message(messageText));
        }

        public void SendMessage([NotNull]Message message)
        {
            Messages.Push(message);
            OnReceived?.Invoke(message);
        }

        public Message ReadLastMessage()
        {
            if (Messages.Count < 1)
                return null;
            return Messages.Pop();
        }

        public MessageType? LastMessageType()
        {
            if (Messages.Count == 0)
                return null;
            else
            {
                return Messages.Peek().Type;
            }
        }

        public void ClearAllMessages()
        {
            Messages.Clear();
        }
    }

    public class MessageProviderSingleton
    {
        
    }
}