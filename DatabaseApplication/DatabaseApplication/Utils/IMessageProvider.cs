﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApplication.Utils
{
    public interface IMessageProvider
    {
        Stack<Message> Messages { get; }

        event Action<Message> OnReceived;
        void SendMessage(Message message);
        Message ReadLastMessage();
        void ClearAllMessages();
    }
}
