﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DatabaseApplication.Utils
{
    public static class SettingsProvider
    {
        public static string ReadSetting(string key)
        {
            var s = Properties.Settings.Default.PrevUser;
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(s);
            if (dict!=null && dict.ContainsKey(key))
                return dict[key];
            else
                return null;

        }


        public static void AddUpdateAppSettings(string key, string value)
        {
            var s = Properties.Settings.Default.PrevUser;
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(s);
            if (dict==null)
                dict = new Dictionary<string, string>();
            dict[key] = value;
            s= JsonConvert.SerializeObject(dict);
            Properties.Settings.Default.PrevUser = s;
            Properties.Settings.Default.Save();

        }
    }
    [SettingsSerializeAs(SettingsSerializeAs.Xml)]
    public class PreviousUserSetting
    {
        public Dictionary<string, string> UsersData { get; set; }
    }
}