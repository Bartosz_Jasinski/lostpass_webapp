﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApplication.crypto
{
    interface ISecureEncryptor
    {
        /// <summary>
        /// Metoda do szyfrowania tekstu
        /// </summary>
        /// <param name="input">To co szyfrujemy</param>
        /// <param name="password">Klucz do szyfrowania</param>
        /// <returns>Zaszyfrowany tekst w base64</returns>
        string EncryptSecureText(SecureString input, string password);

        /// <summary>
        /// Metoda do szyfrowania tekstu
        /// </summary>
        /// <param name="input">To co odszyfrujemy w base64</param>
        /// <param name="password">Klucz do szyfrowania</param>
        /// <returns>Odszyfrowany tekst</returns>
        byte[] DecryptSecureText(string input, string password);
    }
}
