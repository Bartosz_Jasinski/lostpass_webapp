﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatabaseApplication.io.entities;
using DatabaseApplication.Net;


namespace DatabaseApplication.gui.Main
{
    public static class PagesListFactory
    {
        public static ICollection<Page> Pages;

        public static IEnumerable<string> GetPages()
        {
            return Pages?.Select(page => page.Name);
        }
    }
}