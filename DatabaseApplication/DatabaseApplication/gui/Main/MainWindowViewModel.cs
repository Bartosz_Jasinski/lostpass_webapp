﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Castle.Core.Internal;
using DatabaseApplication.Annotations;
using DatabaseApplication.crypto;
using DatabaseApplication.gui.Main.Password_Generator;
using DatabaseApplication.io;
using DatabaseApplication.io.entities;

using DatabaseApplication.Net;
using DatabaseApplication.Utils;
using MaterialDesignThemes.Wpf;
using Page = DatabaseApplication.io.entities.Page;

namespace DatabaseApplication.gui.Main.Vault
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            ForgetSelectedPassword = new SimpleCommand(Forget);
            ChangeSelectedPassword = new TwoParamCommand<int, string>(ChangeSelectedPass);

            DelPage = new Command<int>(DeletePage);
            EditName = new TwoParamCommand<int, string>(EditPageName);
            DelPassword = new Command<int>(DeletePassword);
            ReadMessage = new SimpleCommand(Read);
            OpenLeftDrawer = new SimpleCommand(() => IsLeftDrawerOpen = !IsLeftDrawerOpen);
            CopyToClipboard = new SimpleCommand2<string>(s => Clipboard.SetDataObject(s.Decrypt()));
            MessageProvider.Instance.OnReceived += message =>
            {
                if (message.Type == MessageType.ValidationError)
                    DialogMessage = message.Data;
                if (message.Type == MessageType.Other)
                {
                    MessageProvider.Instance.ReadLastMessage();
                    OtherMessage = message.Data;
                }
            };
            GetConfigs();
            UpdatePages();
        }

 

        private static async Task GetConfigs()
        {
            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                await UserConfigs.GetUserConfigs(client);
            }
        }

        #region Fields

        private ICollection<Page> _pages;

        private User _currentUser;

        private string _dialogMessage;

        private string _searchString;

        private string _url;

        private Password _selectedPassword = new Password() {Page = new Page()};
        private string _otherMessage;
        private bool _isLeftDrawerOpen;
        private HttpData httpData = new HttpData();

        #endregion

        #region Properites

        public string DialogMessage
        {
            get { return _dialogMessage; }
            set
            {
                _dialogMessage = value;
                OnPropertyChanged();
            }
        }

        public bool IsLeftDrawerOpen
        {
            get { return _isLeftDrawerOpen; }
            set
            {
                _isLeftDrawerOpen = value;
                OnPropertyChanged();
            }
        }

        public User CurrentUser
        {
            get { return _currentUser; }
            set
            {
                _currentUser = value;
                //CheckTooFrequentPasswords();
                OnPropertyChanged();
            }
        }

        public ICollection<Page> Pages
        {
            get { return _pages; }
            set
            {
                _pages = value;
                CheckTooFrequentPasswords();
                PagesListFactory.Pages = _pages;
                OnPropertyChanged();
            }
        }


        public ICommand DelPage { get; }

        public ICommand DelPassword { get; }

        public ICommand EditName { get; }

        public ICommand ChangeSelectedPassword { get; }

        public ICommand ForgetSelectedPassword { get; }
        public ICommand ReadMessage { get; set; }
        public ICommand OpenLeftDrawer { get; set; }

        public ICommand CopyToClipboard { get; set; }

        public Password SelectedPassword
        {
            get { return _selectedPassword; }
            set
            {
                _selectedPassword = value;
                OnPropertyChanged();
            }
        }

        public string OtherMessage
        {
            get { return _otherMessage; }
            set
            {
                _otherMessage = value;
                OnPropertyChanged();
            }
        }

        public string SearchString
        {
            get { return _searchString; }
            set
            {
                _searchString = value;
                OnPropertyChanged();
            }
        }

        public string Url
        {
            get { return _url; }
            set
            {
                _url = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Methods

     
        private async void EditPageName(int id, string name)
        {
            Page p = Pages.FirstOrDefault(page => page.PageID == id);
            p.Name = name;
            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                await httpData.EditPage(p, client);
            }
        }

        private async void CheckTooFrequentPasswords()
        {
            await Task.Factory.StartNew(() =>
            {
                var rand = new Random();
                //With 20% probability analyze passwords frequency
                if (rand.Next(5) < 4)
                    return;
                PasswordAnalyser pa = new PasswordAnalyser(Pages);
                pa.CheckIfAnyPasswordUsedToOften();
            });
        }

        private async Task DeletePage(int id)
        {
            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                await httpData.DeletePage(id, client);
            }
            await UpdatePages();
        }

        private async Task DeletePassword(int id)
        {
            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                await httpData.DeletePassword(id, client);
            }
            await UpdatePages();
        }

        private void Read()
        {
            OtherMessage = "";
            if (MessageProvider.Instance.LastMessageType() != null &&
                MessageProvider.Instance.LastMessageType().Equals(MessageType.Other))
                OtherMessage = MessageProvider.Instance.ReadLastMessage().Data;
        }

        private void Forget()
        {
            SelectedPassword = new Password() {Page = new Page()};
            Url = "";
        }



        private async void ChangeSelectedPass(int id, string url)
        {
            SelectedPassword =
                Pages.SelectMany(page => page.Passwords).FirstOrDefault(password => password.PasswordID == id);

            if (SelectedPassword==null)
                return;
            
            SelectedPassword.Pass = SelectedPassword.Pass.Decrypt();
            SelectedPassword.UserLogin = SelectedPassword.UserLogin.Decrypt();
            
            OnPropertyChanged("SelectedPassword");

            Url = url;
            await UpdatePages();
        }

        //public List<string> PageNames { get; set; }


        public async void AddNewPage(Page p)
        {
            AesEncryptor enc = new AesEncryptor();
            if (Validate()) return;
            var t = new Page()
            {
                Name = Url,
                URL = Url,
            };
            t.Passwords.Add(new Password()
            {
                Pass = SelectedPassword.Pass.Encrypt(),
                UserLogin = SelectedPassword.UserLogin.Encrypt()
            });
            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                await httpData.AddPage(t, client);
            }
            await UpdatePages();
        }

        private async Task UpdatePages()
        {
            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                var p = await httpData.GetAllPages(client);
                Thread.Sleep(500);
                Pages = p.Select(pp => pp.ToPage()).ToList();
            }
        }

        public async void AddPassword(Page p)
        {
            AesEncryptor enc = new AesEncryptor();
            if (Validate()) return;
            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                await httpData.AddPassword(new Password()
                {
                    PageID = p.PageID,
                    Pass = SelectedPassword.Pass.Encrypt(),
                    UserLogin = SelectedPassword.UserLogin.Encrypt()
                }, client);
            }
            await UpdatePages();
        }

        public async Task<bool> OnPasswordAdded()
        {
            if (Validate())
            {
                MessageProvider.Instance.ReadLastMessage();
                return false;
            }
            var data = httpData;


            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                if (await data.PasswordExists(SelectedPassword.PasswordID, client))
                {
                    await DeletePassword(SelectedPassword.PasswordID);
                }
            }

            var p = Pages.FirstOrDefault(page => page.Name == Url);
            if (p != null)
            {
                AddPassword(p);
            }
            else
            {
                AddNewPage(p);
            }

            //CheckTooFrequentPasswords();
            if (MessageProvider.Instance.LastMessageType() == MessageType.ValidationError)
            {
                MessageProvider.Instance.ReadLastMessage();
                Forget();
                return false;
            }
            Forget();
            return true;
        }


        private bool Validate()
        {
            if (SelectedPassword.Pass.IsNullOrEmpty() || SelectedPassword.UserLogin.IsNullOrEmpty() ||
                Url.IsNullOrEmpty())
            {
                MessageProvider.Instance.SendMessage(new Message("Empty fields", MessageType.ValidationError));
                return true;
            }
            return false;
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}