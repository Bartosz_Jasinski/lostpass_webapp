﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Castle.Core.Internal;
using DatabaseApplication.crypto;
using DatabaseApplication.io.entities;

using DatabaseApplication.Utils;

namespace DatabaseApplication.gui.Main
{
    

    public class PassStrengthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return PasswordAnalyser.PasswordStrength(value as string);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IntToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int x = PasswordAnalyser.PasswordStrength(value as string);

            return new SolidColorBrush(Color.FromRgb((byte)((2.0f * x)*255), (byte)((2.0f * (1-x)) * 255), 0));

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class NullToVisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((string) value).IsNullOrEmpty() ? Visibility.Hidden : Visibility.Visible;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BoolToInvisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool) value ? Visibility.Hidden : Visibility.Visible;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class SearchFilterConverter : IMultiValueConverter
    {
        

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var pages = values[0] as IEnumerable<Page>;
            if (pages == null)
                return null;
            var search = values[1] as string;
            if (!string.IsNullOrEmpty(search))
            {
                pages = pages.Where(page => page.Name.Contains(search));
            }

            var ord = SortOrder.Order;
            switch (ord)
            {
                case Order.ModificationAsc:
                    return pages.Reverse();
                case Order.ModificationDesc:
                    return pages;
                case Order.CreateAsc:
                    return pages.Reverse();
                case Order.CreateDesc:
                    break;
                case Order.NameAsc:
                    return pages.OrderBy(page => page.Name);
                case Order.NameDesc:
                    return pages.OrderByDescending(page => page.Name);
            }
            return pages;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ParamsConverter : IMultiValueConverter
    {


        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values.Clone();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class DecryptConverter : IValueConverter
    {


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = ((string) value).Decrypt();
            if (UserConfigs.Settings!=null && UserConfigs.Settings.HidePasswords)
                return String.Concat(Enumerable.Repeat("•", s.Length));
            else return s;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}