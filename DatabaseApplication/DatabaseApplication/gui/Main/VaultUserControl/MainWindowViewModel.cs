﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using DatabaseApplication.Annotations;
using DatabaseApplication.io.entities;
using DatabaseApplication.io.Page;
using DatabaseApplication.io.User;

namespace DatabaseApplication.gui.Main.VaultUserControl
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public User CurrentUser
        {
            get { return _currentUser; }
            set
            {
                _currentUser = value;
                PagesListFactory.User = _currentUser.UserLogin;
                using (UserRepository ur = new UserRepository(new DB()))
                {
                   Pages = ur.FindByUserLogin(CurrentUser.UserLogin).Pages;

                }
                OnPropertyChanged();
            }
        }
        

        private ICollection<Page> _pages;
        private User _currentUser;

        public ICollection<Page> Pages
        {
            get { return _pages; }
            set
            {
                _pages = value;
                PageNames = value.Select(page => page.Name).ToList();
                OnPropertyChanged();
            }
        }

        public List<string> PageNames { get; set; }

        public string Url { get; set; }
        public Password SelectedPassword { get; set; } = new Password() {Page = new Page()};

        public void AddNewPage(Page p)
        {
            using (var db = new DB())
            {
                var t = new Page()
                {
                   
                    //ID = db.Pages.Max(page => page.ID) + 1,
                    Name = Url,
                    URL = Url,
                };
                t.Passwords.Add(new Password()
                {
                    
                    //ID = db.Passwords.Max(password => password.ID) + 1,
                    Pass = SelectedPassword.Pass,
                    UserLogin = SelectedPassword.UserLogin
                });


                db.Users.First(user => user.UserLogin == CurrentUser.UserLogin).Pages.Add(t);
                db.SaveChanges();
                Pages = db.Users.First(user => user.UserLogin == CurrentUser.UserLogin).Pages;
            }
        }

        public void AddPassword(Page p)
        {
            using (var pr = new PageRepository(new DB()))
            {
                pr.GetPageByID(p.PageID).Passwords.Add(new Password()
                {
                    Pass = SelectedPassword.Pass,
                    UserLogin = SelectedPassword.UserLogin
                });
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
