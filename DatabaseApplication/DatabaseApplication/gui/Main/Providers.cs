﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DatabaseApplication.io.entities;
using WpfControls.Editors;

namespace DatabaseApplication.gui.Main
{
    internal static class StringExtensions
    {
        public static bool Contains(this String str, String substring,
                                    StringComparison comp)
        {

            if (str == null || substring == null)
                return false;

            return str.IndexOf(substring, comp) >= 0;
        }
    }
    public class SuggestionProvider : ISuggestionProvider
    {
        public IEnumerable<string> ListOfPages { get; set; }

        public IEnumerable GetSuggestions(string filter)
        {
            ListOfPages = PagesListFactory.GetPages();
            if (string.IsNullOrWhiteSpace(filter) || ListOfPages==null) return null;
            return
                ListOfPages
                    .Where(s => s.Contains(filter, StringComparison.CurrentCultureIgnoreCase ))
                    .ToList();

        }

        public SuggestionProvider()
        {
            ListOfPages = PagesListFactory.GetPages();
        }
    }
}