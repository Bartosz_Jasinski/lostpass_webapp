﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApplication.gui.Main
{
    public enum Order
    {
        ModificationAsc, ModificationDesc, CreateAsc, CreateDesc, NameAsc, NameDesc
    }
    public static class SortOrder
    {
        public static Order Order;

        public static void SetOrder(string s)
        {
            switch (s)
            {
                case "Time Edited - descending":
                    Order=Order.ModificationDesc;
                    break;
                case "Time Edited - ascending":
                    Order=Order.ModificationAsc;
                    break;
                case "Name - descending":
                    Order=Order.NameDesc;
                    break;
                case "Name - ascending":
                    Order=Order.NameAsc;
                    break;
                case "Time Added (newest first)":
                    Order=Order.CreateAsc;
                    break;
                case "Time Added (oldest first)":
                    Order = Order.CreateDesc;
                    break;

            }
        }
    }
}
