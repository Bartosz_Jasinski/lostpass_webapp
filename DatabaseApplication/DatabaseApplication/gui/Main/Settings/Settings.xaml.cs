﻿using System.Windows.Controls;
using DatabaseApplication.Utils;

namespace DatabaseApplication.gui.Main.Settings
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : UserControl
    {
        public Settings()
        {
            InitializeComponent();
            if (UserConfigs.Settings != null)
            {
                ComboBox.SelectedItem = UserConfigs.Settings.SortOrder;
                SortOrder.SetOrder(UserConfigs.Settings.SortOrder);
            }
            UserConfigs.SettingsChanged += () =>
            {
                if (ComboBox.SelectedItem.ToString() != UserConfigs.Settings.SortOrder)
                {
                    ComboBox.Text = UserConfigs.Settings.SortOrder;
                    SortOrder.SetOrder(UserConfigs.Settings.SortOrder);
                }
            };

        }

        private async void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox != null)
            {
                var comboBoxItem = comboBox.SelectedItem as ComboBoxItem;
                if (comboBoxItem != null && comboBoxItem.Content!=null)
                {
                    var s = comboBoxItem.Content.ToString();
                    SortOrder.SetOrder(s);
                    UserConfigs.Settings.SortOrder = s;
                    await UserConfigs.Sync();
                }
            }
        }
    }


}