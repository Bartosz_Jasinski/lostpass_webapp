﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using DatabaseApplication.Annotations;
using DatabaseApplication.Utils;
using MaterialDesignColors;

namespace DatabaseApplication.gui.Main.Settings
{
    class SettingsViewModel : INotifyPropertyChanged
    {
        public SettingsViewModel()
        {
            if (UserConfigs.Settings != null)
            {
                Dark = UserConfigs.Settings.ColorPalette;
                Hide = UserConfigs.Settings.HidePasswords;
            }
            UserConfigs.SettingsChanged += () =>
            {
                if (UserConfigs.Settings.ColorPalette!=Dark)
                    Dark = UserConfigs.Settings.ColorPalette;
                Hide = UserConfigs.Settings.HidePasswords;
            };

        }
        private bool _dark = true;
        private bool _hide = true;

        public bool Dark
        {
            get { return _dark; }
            set
            {
                _dark = value;
                ChangeColor(_dark);
                UserConfigs.Settings.ColorPalette = value;
                UserConfigs.Sync();
                OnPropertyChanged();
            }
        }

        public bool Hide
        {
            get { return _hide; }
            set
            {
                _hide = value;
                UserConfigs.Settings.HidePasswords = value;
                UserConfigs.Sync();
                OnPropertyChanged();
            }
        }

        public static void ChangeColor(bool _dark)
        {
            new PaletteHelper().SetLightDark(_dark);
            new PaletteHelper().ReplacePrimaryColor(_dark ? "yellow" : "indigo");
            new PaletteHelper().ReplaceAccentColor(_dark ? "yellow" : "indigo");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
