﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DatabaseApplication.gui.Main.Delete_Account
{
    class DeleteValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null)
                return new ValidationResult(false, "");
            if (value.ToString() == "Yes, I want to delete my account.")
                return ValidationResult.ValidResult;
            else
            {
                return new ValidationResult(false, "");
            }
        }
    }
}
