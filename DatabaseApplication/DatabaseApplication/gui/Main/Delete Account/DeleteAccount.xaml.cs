﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DatabaseApplication.login;
using DatabaseApplication.Net;

namespace DatabaseApplication.gui.Main.Delete_Account
{
    /// <summary>
    /// Interaction logic for DeleteAccount.xaml
    /// </summary>
    public partial class DeleteAccount : UserControl
    {
        public DeleteAccount()
        {
            InitializeComponent();


        }

        private async void DelButton_OnClick(object sender, RoutedEventArgs e)
        {
            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                var b = await Login.DeleteAccount(client);
                if (b)
                {
                    Login.Logout();
                    OpenStartWindow();
                }

            }
        }

        private void OpenStartWindow()
        {

            DatabaseApplication.StartWindow.StartWindow mw = new DatabaseApplication.StartWindow.StartWindow();
            var w = Window.GetWindow(this);
            mw.Height = w.Height;
            mw.Width = w.Width;
            mw.Left = w.Left;
            mw.Top = w.Top;
            mw.WindowState = w.WindowState;
            w.Close();

            mw.Show();
            //await ((MainWindowViewModel)mw.DataContext).SetUser(usr);
        }
    }
}
