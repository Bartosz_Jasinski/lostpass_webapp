﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DatabaseApplication.gui.Main
{
    class Command<T> : ICommand
    {
        private readonly Func<T,Task> _task;
        public Command(Func<T, Task> task)
        {
            _task = task;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _task((T) parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
    class SimpleCommand : ICommand
    {
        private readonly Action _task;
        public SimpleCommand(Action task)
        {
            _task = task;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _task();
        }

        public event EventHandler CanExecuteChanged;
    }
    class SimpleCommand2<T> : ICommand
    {
        private readonly Action<T> _task;
        public SimpleCommand2(Action<T> task)
        {
            _task = task;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _task((T)parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
    class TwoParamCommand<T,U> : ICommand
    {
        private readonly Action<T,U> _task;
        public TwoParamCommand(Action<T,U> task)
        {
            _task = task;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var parametres = (object[])parameter;
            _task((T) parametres[0], (U) parametres[1]);
        }

        public event EventHandler CanExecuteChanged;
    }
    class ThreeParamCommand<T, U, V> : ICommand
    {
        private readonly Action<T, U, V> _task;
        public ThreeParamCommand(Action<T, U, V> task)
        {
            _task = task;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var parametres = (object[])parameter;
            _task((T)parametres[0], (U)parametres[1], (V)parametres[2]);
        }

        public event EventHandler CanExecuteChanged;
    }
}
