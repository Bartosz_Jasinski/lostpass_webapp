﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using DatabaseApplication.Annotations;
using DatabaseApplication.io.entities;
using DatabaseApplication.login;
using DatabaseApplication.Utils;

namespace DatabaseApplication.StartWindow
{
    public class LoginContext : INotifyPropertyChanged
    {
        private string _message;

        public LoginContext()
        {
            MessageProvider.Instance.OnReceived += message => Message = message.ToString();
        }

        public string UserName { get; set; }
        public SecureString Password { get; set; }

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                OnPropertyChanged();
            }
        }

        public bool Remember
        {
            get; set;
        }

        public string CheckRememberedLogin()
        {
            var s = Properties.Settings.Default.RememberToken;
            return s;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void RememberUser()
        {
            if (!Remember)
                return;
            Properties.Settings.Default.RememberToken = Login.Token;
            Properties.Settings.Default.RememberEmail = UserName;
            Properties.Settings.Default.Save();
        }
    }
}