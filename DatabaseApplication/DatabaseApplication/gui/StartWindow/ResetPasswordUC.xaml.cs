﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DatabaseApplication.login;
using DatabaseApplication.Net;
using DatabaseApplication.Utils;
using SettingsProvider = DatabaseApplication.Utils.SettingsProvider;

namespace DatabaseApplication.gui.StartWindow
{
    /// <summary>
    /// Interaction logic for ResetPasswordUC.xaml
    /// </summary>
    public partial class ResetPasswordUC : UserControl
    {
        
        private DatabaseApplication.StartWindow.StartWindow parent;

        public ResetPasswordUC()
        {
            InitializeComponent();
            parent = ((DatabaseApplication.StartWindow.StartWindow)Window.GetWindow(this));
        }

        private async void GetCodeButton_OnClick(object sender, RoutedEventArgs e)
        {
            CodeButton.IsEnabled = false;
            var hash = SettingsProvider.ReadSetting(UserTB.Text);
            if (hash == null)
            {
                if (parent == null)
                    parent = ((DatabaseApplication.StartWindow.StartWindow)Window.GetWindow(this));
                var res = MessageBox.Show(parent,
                    "If you reset password on this mashine, ALL of your passwords will be LOST!!!\n If you have loggen in on other mashine please try to reset password there.\n\nContinue?",
                    "Warning",
                    MessageBoxButton.YesNo);
                if (res == MessageBoxResult.No)
                {
                    var newCont = new LoginUC();
                    newCont.UserTB = this.UserTB;
                   
                    parent.ContentControl.Content = newCont;
                    return;
                }
            }
            var b = await Login.ResetPassword(UserTB.Text);
            CodeButton.IsEnabled = true;
            if (!b)
                return;
            UserTB.IsEnabled = false;
            CodeButton.Visibility = Visibility.Collapsed;
            NewPassPanel.Visibility = Visibility.Visible;
        }

        private async void ResetButton_OnClick(object sender, RoutedEventArgs e)
        {
            //TODO IMPORTANT Get password hash again and encrypt passwords
            if (PasswordTB.Password != PasswordConfTB.Password)
            {
                MessageProvider.Instance.SendMessage("Passwords are different");
            }
            if (parent == null)
                parent = ((DatabaseApplication.StartWindow.StartWindow)Window.GetWindow(this));
            var hash = SettingsProvider.ReadSetting(UserTB.Text);
            if (hash == null)
            {

                var res = MessageBox.Show(parent,
                    "I'm serious. You will lose everyching.\nShure?",
                    "Warning",
                    MessageBoxButton.YesNo);
                if (res == MessageBoxResult.No)
                {
                    var newCont = new LoginUC();
                    newCont.UserTB = this.UserTB;
                    parent.ContentControl.Content = newCont;
                    return;
                }
            }
            Reset.IsEnabled = false;
            var b = await Login.ConfirmReset(UserTB.Text, CodeTB.Text, PasswordTB.Password);
            Reset.IsEnabled = true;
            if (b)
            {
                var newCont = new LoginUC();
                
                newCont.UserTB = this.UserTB;
                
                parent.PasswordChanged = true;
                parent.OldHash = hash;
                parent.ContentControl.Content = newCont;
            }
        }

    }
}
