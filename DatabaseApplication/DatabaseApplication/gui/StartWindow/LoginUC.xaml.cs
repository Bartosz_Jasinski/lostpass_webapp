﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DatabaseApplication.crypto;
using DatabaseApplication.gui.Main;
using DatabaseApplication.io.entities;
using DatabaseApplication.login;
using DatabaseApplication.Net;
using DatabaseApplication.Properties;
using DatabaseApplication.StartWindow;
using MaterialDesignThemes.Wpf;
using SettingsProvider = DatabaseApplication.Utils.SettingsProvider;
using System.ComponentModel.DataAnnotations;
using DatabaseApplication.Utils;
using MahApps.Metro.Controls;

namespace DatabaseApplication.gui.StartWindow
{
    /// <summary>
    /// Interaction logic for LoginUC.xaml
    /// </summary>
    public partial class LoginUC : UserControl
    {
        private DatabaseApplication.StartWindow.StartWindow parent;

        public LoginUC()
        {
            InitializeComponent();
            parent = ((DatabaseApplication.StartWindow.StartWindow) Window.GetWindow(this));
        }
        private async void LogInButtonClick(object sender, RoutedEventArgs e)
        {
            await Login();
        }

        private async Task Login()
        {
            if (parent == null)
                parent = ((DatabaseApplication.StartWindow.StartWindow)Window.GetWindow(this));
            LoginButton.IsEnabled = false;
            SignupButton.IsEnabled = false;
            HttpResponseMessage u = null;

            UserTB.Text = UserTB.Text.Trim();
            if (!DatabaseApplication.login.Login.IsEmailValid(UserTB.Text))
            {
                MessageProvider.Instance.SendMessage("Email is not valid");
                return;
            }

            try
            {
                u = await login.Login.Authenticate(UserTB.Text, PasswordTB.Password);
            }
            catch (AccountNotConfirmedException)
            {
                await DialogHost.Show(this.DialogHost.DialogContent);
            }
            LoginButton.IsEnabled = true;
            SignupButton.IsEnabled = true;
            if (u == null)
                return;
            (DataContext as LoginContext).RememberUser();
            //login succeed
            PasswordHash.Key = PasswordHash.GetPasswordHash(PasswordTB.SecurePassword);
            SavePasswordHash();
            //TODO check password change after app restart
            if (parent.OldHash != null)
            {
                using (var client = HTTPClientConfig.CreateClientForCurrentUser())
                {
                    await new HttpData().RecryptAll(parent.OldHash, client);
                }
            }
            OpenMainWindow(null);
        }
        
        private void SavePasswordHash()
        {
            SettingsProvider.AddUpdateAppSettings(UserTB.Text, PasswordHash.Key);
        }

        

        private void OpenMainWindow(User usr)
        {

            MainWindow mw = new MainWindow();
            var w = Window.GetWindow(this);
            mw.Height = w.Height;
            mw.Width = w.Width;
            mw.Left = w.Left;
            mw.Top = w.Top;
            mw.WindowState = w.WindowState;
            w.Close();

            mw.Show();
            //await ((MainWindowViewModel)mw.DataContext).SetUser(usr);
        }

        private void SignupButton_OnClick(object sender, RoutedEventArgs e)
        {
            var newContent = new RegisterUC();
            newContent.UserTB.Text = UserTB.Text;
            newContent.PasswordTB.Password = PasswordTB.Password;
            ((DatabaseApplication.StartWindow.StartWindow)Window.GetWindow(this)).ContentControl.Content = newContent;
        }

        private async void ConfButton_OnClick(object sender, RoutedEventArgs e)
        {
            bool b = await login.Login.ConfirmAccount(CodeTB.Text, UserTB.Text);
            if (!b)
            {
                WrCode.Visibility = Visibility.Visible;
                return;
            }
            await Login();

        }

        private void ForgotPassword_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ((DatabaseApplication.StartWindow.StartWindow)Window.GetWindow(this)).ContentControl.Content = new ResetPasswordUC();
        }
        
    }

}