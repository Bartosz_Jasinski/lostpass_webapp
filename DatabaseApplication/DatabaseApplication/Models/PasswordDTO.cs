﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseApplication.io.entities;

namespace DatabaseApplication.Models
{
    public class PasswordDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public int PageID { get; set; }
        public int PasswordID { get; set; }

        public Password ToPassword(int pageID=0)
        {
            return new Password()
            {
                UserLogin = this.Login,
                Pass = this.Password,
                PageID = pageID==0?this.PageID:pageID,
                PasswordID = this.PasswordID
            };
        }
    }
}
