﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using DatabaseApplication.login;
//using System;
//using System.Collections.Generic;
//using System.Data.Entity.Validation;
//using System.Diagnostics.Eventing.Reader;
//using System.Linq;
//using System.Security;
//using System.Text;
//using System.Threading.Tasks;
//using DatabaseApplication.crypto;
//using DatabaseApplication.io;
//using DatabaseApplication.io.entities;
//using DatabaseApplication.io.Page;
//using DatabaseApplication.io.Password;
//using DatabaseApplication.io.User;

//using DatabaseApplication.Utils;
//using Moq;

//namespace DatabaseApplication.login.Tests
//{
//    [TestClass()]
//    public class LoginTests
//    {
//        private static SecureString getSS(string s)
//        {
//            SecureString ss = new SecureString();
//            foreach (var c in s)
//            {
//                ss.AppendChar(c);
//            }
//            return ss;
//        }
//        class TestableUoWFactory : IUnitOfWorkFactory
//        {
//            private User u;

//            public TestableUoWFactory(User u)
//            {
//                this.u = u;
//            }

//            public IUnitOfWork GetUnitOfWork()
//            {
//                return new TestableUnitOfWork(u);
//            }


//        }
//        class TestableUnitOfWork : IUnitOfWork
//        {

//            public TestableUnitOfWork(User u)
//            {
//                var mock = new Mock<IUserRepository>();
//                AesEncryptor enc = new AesEncryptor();
//                bool noonesaved = false;
//                mock.Setup(r => r.FindByUserLogin("admin")).Returns(new User()
//                {
//                    UserLogin = "admin",
//                    PassHash = PasswordHash.GetPasswordHash(getSS("admin"))
//                });
//                mock.Setup(r => r.SaveChanges()).Returns(() =>
//                {
//                    noonesaved = true;
//                    return 0;
//                });

//                mock.Setup(r => r.Add(u)).Returns(() =>
//                {
//                    if (this.Users.FindByUserLogin(u.UserLogin) == null)
//                        return RepoSignals.RecordAdded;
//                    return RepoSignals.RecordExists;
//                });
//                mock.Setup(r => r.FindByUserLogin("noone")).Returns(() => !noonesaved ? null : u);
//                Users = mock.Object;
//            }

//            public void Dispose()
//            {

//            }

//            public IUserRepository Users { get; set; }
//            public IPasswordRepository Passwords { get; set; }
//            public IPageRepository Pages { get; set; }
//        }

//        [TestMethod()]
//        public void LoginSuccedeTest()
//        {
//            User u = new User() { UserLogin = "admin", PassHash = PasswordHash.GetPasswordHash(getSS("admin")) };

//            var res = Login.Autenticate(u, new TestableUoWFactory(u));
//            Assert.IsNotNull(res);
//            Assert.IsTrue(MessageProvider.Instance.LastMessageType() == null || MessageProvider.Instance.LastMessageType() != MessageType.LoginError);
//            Assert.AreEqual(res.UserLogin, u.UserLogin);
//        }

//        [TestMethod]
//        public void LoginFailTest()
//        {
//            User u = new User() { UserLogin = "noone", PassHash = PasswordHash.GetPasswordHash(getSS("qwerty")) };
//            var res = Login.Autenticate(u, new TestableUoWFactory(u));
//            Assert.IsNull(res);
//            Assert.AreEqual(MessageProvider.Instance.ReadLastMessage().Type, MessageType.LoginError);
//        }

//        [TestMethod]
//        public void EmptyUsernameOrPassword()
//        {
//            User u = new User();
//            User u1 = new User() { UserLogin = "asdsa" };
//            User u2 = new User() { PassHash = "dasd" };
//            User res;
//            res = Login.Autenticate(u);
//            Assert.IsNull(res);
//            Assert.AreEqual(MessageProvider.Instance.ReadLastMessage().Type, MessageType.LoginError);

//            res = Login.Autenticate(u1);
//            Assert.IsNull(res);
//            Assert.AreEqual(MessageProvider.Instance.ReadLastMessage().Type, MessageType.LoginError);

//            res = Login.Autenticate(u2);
//            Assert.IsNull(res);
//            Assert.AreEqual(MessageProvider.Instance.ReadLastMessage().Type, MessageType.LoginError);

//        }

//        [TestMethod()]
//        public void CreateNewUserOkTest()
//        {
//            User u = new User() { UserLogin = "noone", PassHash = PasswordHash.GetPasswordHash(getSS("qwerty")) };
//            var res = Login.CreateNewUser(u, new TestableUoWFactory(u));
//            Assert.IsNotNull(res);
//        }

//        [TestMethod, ExpectedException(typeof(DbEntityValidationException))]
//        public void CreateUserFailTest()
//        {
//            User u = new User() { UserLogin = "admin", PassHash = "qwerty" };
//            var res = Login.CreateNewUser(u, new TestableUoWFactory(u));
//            Assert.IsNull(res);
//        }

//        [TestMethod(), ExpectedException(typeof(ArgumentNullException))]
//        public void NullUserCreate()
//        {
//            Login.CreateNewUser(null);
//        }
//    }
//}