﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseApplication.crypto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApplication.crypto.Tests
{
    [TestClass()]
    public class PasswordHasherTests
    {
        private SecureString getSS(string s)
        {
            SecureString ss = new SecureString();
            foreach (var c in s)
            {
                ss.AppendChar(c);
            }
            return ss;
        }
        [TestMethod()]
        public void GetPasswordHashNullEmptyTest()
        {
            Assert.AreEqual(PasswordHash.GetPasswordHash(null), "");
            Assert.AreEqual(PasswordHash.GetPasswordHash(new SecureString()), "");
        }
        [TestMethod]
        public void GetPasswordHashTest()
        {
            var s = PasswordHash.GetPasswordHash(getSS("sadsadsadsad"));
            Assert.IsNotNull(s);
            Assert.IsTrue(s.Length>0);
        }
    }
}