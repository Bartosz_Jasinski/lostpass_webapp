﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseApplication.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using DatabaseApplication.crypto;
using DatabaseApplication.io.entities;


namespace DatabaseApplication.Utils.Tests
{
    [TestClass()]
    public class PasswordAnalyserTests
    {

        private SecureString getSS(string s)
        {
            SecureString ss = new SecureString();
            foreach (var c in s)
            {
                ss.AppendChar(c);
            }
            return ss;
        }


        //[TestMethod()]
        //public void NoFrequentPasswordTest()
        //{
        //    AesEncryptor e = new AesEncryptor();
        //    var t = PasswordHash.GetPasswordHash(getSS("fdsadsad"));
        //    var user = new User()
        //    {
        //        Pages = Enumerable.Range(1, 40).Select(i => new Page()
        //        {
        //            PageID = i,
        //            Passwords = new List<Password>()
        //            {
        //                new Password()
        //                {
        //                    PasswordID = 0,
        //                    Pass = e.EncryptText(Enumerable.Range(0, i).Select(i1 => "a").Aggregate((c, c1) => ""+c+c1), t)
        //                }
        //            }
        //        }).ToList(),
        //        PassHash = t
        //    };
        //    var pa = new PasswordAnalyser(user, new TestableUoWFactory(user));
        //    Assert.AreEqual(pa.CheckIfAnyPasswordUsedToOften(), false);
        //}

        //[TestMethod()]
        //public void NotEnoughPasswordsTest()
        //{
        //    AesEncryptor e = new AesEncryptor();
        //    var t = PasswordHash.GetPasswordHash(getSS("fdsadsad"));
        //    var user = new User()
        //    {
        //        Pages = Enumerable.Range(1, 3).Select(i => new Page()
        //        {
        //            PageID = i,
        //            Passwords = new List<Password>()
        //            {
        //                new Password()
        //                {

        //                    Pass = e.EncryptText("aaa",t)
        //                },
        //                new Password()
        //                {

        //                    Pass = e.EncryptText("aaa",t)
        //                },
        //                new Password()
        //                {

        //                    Pass = e.EncryptText("aaa",t)
        //                }
        //            }
        //        }).ToList(),
        //        PassHash = t
        //    };
        //    var pa = new PasswordAnalyser(user, new TestableUoWFactory(user));
        //    Assert.AreEqual(pa.CheckIfAnyPasswordUsedToOften(), false);
        //}
        //[TestMethod()]
        //public void OnlyOnePasswordTest()
        //{
        //    AesEncryptor e = new AesEncryptor();
        //    var t = PasswordHash.GetPasswordHash(getSS("fdsadsad"));
        //    var user = new User()
        //    {
        //        Pages = Enumerable.Range(1, 10).Select(i => new Page()
        //        {
        //            PageID = i,
        //            Passwords = new List<Password>()
        //            {
        //                new Password()
        //                {

        //                    Pass = e.EncryptText("aaa",t)
        //                },
        //                new Password()
        //                {
        //                    Pass = e.EncryptText("aaa",t)
        //                },
        //                new Password()
        //                {

        //                    Pass = e.EncryptText("aaa",t)
        //                }
        //            }
        //        }).ToList(),
        //        PassHash = t
        //    };
        //    var pa = new PasswordAnalyser(user, new TestableUoWFactory(user));
        //    Assert.AreEqual(pa.CheckIfAnyPasswordUsedToOften(), true);
        //    Assert.IsTrue(MessageProvider.Instance.Messages.Count > 0);
        //}

        [TestMethod()]
        public void PasswordStrengthTest()
        {
            Assert.AreEqual(PasswordAnalyser.PasswordStrength("1aA[]lkl"), 80);
        }

        [TestMethod()]
        public void PasswordStrengthTest1()
        {
            Assert.AreEqual(PasswordAnalyser.PasswordStrength("12345678"), 4);
        }

        [TestMethod()]
        public void PasswordStrengthTest2()
        {
            Assert.AreEqual(PasswordAnalyser.PasswordStrength("qwerty12345"), 55);
        }

        [TestMethod()]
        public void ShortPasswordStrengthTest()
        {
            Assert.IsTrue(PasswordAnalyser.PasswordStrength("1") < 5);
        }
        [TestMethod()]
        public void EmptyPasswordStrengthTest()
        {
            Assert.AreEqual(PasswordAnalyser.PasswordStrength(""), 0);
        }

        [TestMethod(), ExpectedException(typeof(ArgumentNullException))]
        public void PasswordAnalyserTest()
        {
            new PasswordAnalyser(null);
        }
    }
}