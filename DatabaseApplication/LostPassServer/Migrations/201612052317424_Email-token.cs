namespace LostPassServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Emailtoken : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "EmailConfirmationToken", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "EmailConfirmationToken");
        }
    }
}
