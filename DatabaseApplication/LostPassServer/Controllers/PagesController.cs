﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LostPassServer.Data;
using LostPassServer.Models;
using Microsoft.AspNet.Identity;

namespace LostPassServer.Controllers
{
    [Authorize]
    public class PagesController : BaseApiController
    {
        private dotNETDBEntities db = new dotNETDBEntities();

        // GET: api/Pages
        public async Task<IHttpActionResult> GetPages()
        {
            var name = User.Identity.GetUserId();
            var pages = await db.Pages.Where(p => p.Username==name).Include("Passwords").ToListAsync();
            return Ok(pages.Select(p=>new PagesDTO(p, p.Passwords)));
        }

        // GET: api/Pages/5
        [ResponseType(typeof(PagesDTO))]
        public async Task<IHttpActionResult> GetPages(int id)
        {
            var name = User.Identity.GetUserId();
            Pages pages = await db.Pages.FindAsync(id);
            if (pages == null)
            {
                return NotFound();
            }

            return Ok(new PagesDTO(pages, pages.Passwords));
        }

        

        // PUT: api/Pages/5

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPages(int id, Pages pages)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pages.PageID)
            {
                return BadRequest();
            }

            try
            {
                var page = await db.Pages.FirstOrDefaultAsync(p => p.PageID == id);
                if (page == null || page.Username!=User.Identity.GetUserId())
                    return NotFound();

                page.Name = pages.Name;
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PagesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        
        
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> DeletePages()
        {

            var id = User.Identity.GetUserId();
            if (id == null)
                return BadRequest("Unauthorized");
            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser != null)
            {
                IdentityResult result = await this.AppUserManager.DeleteAsync(appUser);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                var usrPages = db.Pages.Where(p => p.Username == id);

                foreach (var usrPage in usrPages)
                {
                    db.Pages.Remove(usrPage);
                }
                var usrConf = db.Configs.Find(id);
                if (usrConf != null)
                    db.Configs.Remove(usrConf);
                await db.SaveChangesAsync();
                return Ok();

            }

            return NotFound();
        }

        // POST: api/Pages
        [ResponseType(typeof(Pages))]
        public async Task<IHttpActionResult> PostPages(Pages pages)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            pages.Username = User.Identity.GetUserId();
            db.Pages.Add(pages);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = pages.PageID }, pages);
        }

        // DELETE: api/Pages/5
        [ResponseType(typeof(Pages))]
        public async Task<IHttpActionResult> DeletePages(int id)
        {
            if (id == -1)
                return await Del();
            Pages pages = await db.Pages.FindAsync(id);
            if (pages == null || pages.Username!= User.Identity.GetUserId())
            {
                return NotFound();
            }

            db.Pages.Remove(pages);
            await db.SaveChangesAsync();

            return Ok(pages);
        }

        private async Task<IHttpActionResult> Del()
        {
            var id = User.Identity.GetUserId();
            if (id == null)
                return BadRequest("Unauthorized");
            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser != null)
            {
                IdentityResult result = await this.AppUserManager.DeleteAsync(appUser);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                var usrPages = db.Pages.Where(p => p.Username == id);

                foreach (var usrPage in usrPages)
                {
                    db.Pages.Remove(usrPage);
                }
                var usrConf = db.Configs.Find(id);
                if (usrConf != null)
                    db.Configs.Remove(usrConf);
                await db.SaveChangesAsync();
                return Ok();

            }

            return NotFound();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PagesExists(int id)
        {
            return db.Pages.Count(e => e.PageID == id) > 0;
        }
    }
}