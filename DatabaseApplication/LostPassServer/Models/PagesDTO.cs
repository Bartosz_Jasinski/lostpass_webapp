﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using LostPassServer.Data;

namespace LostPassServer.Models
{
    [DataContract]
    public class PagesDTO
    {
        public PagesDTO(Pages page, ICollection<Passwords> passes)
        {
            Name = page.Name;
            PageID = page.PageID;
            Passwords = passes.Select(p => new PasswordsDTO() {Login = p.UserLogin, Password = p.Pass, PasswordID = p.PasswordID}).ToList();
        }
        [DataMember]
        public int PageID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public List<PasswordsDTO> Passwords { get; set; }
    }
}